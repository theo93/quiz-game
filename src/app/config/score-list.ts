import { IScoreItem } from "../shared/iscore-item";

export let ScoreList: Array<IScoreItem> = [{
    value: 5000,
  },{
    value: 10000,
  },{
    value: 25000,
  },{
    value: 50000,
  },{
    value: 100000,
  },{
    value: 200000,
  },{
    value: 300000,
  },{
    value: 500000,
  },{
    value: 800000,
  },{
    value: 1500000,
  },{
    value: 3000000,
  },{
    value: 5000000,
  },{
    value: 10000000,
  },{
    value: 20000000,
  },{
    value: 40000000,
  }]