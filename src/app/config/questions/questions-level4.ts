import { Question } from "../../shared/question";
export let Questions = [
    new Question('Hányszor édesebb a szacharin, mint a cukor?','Kb 500-szor','Kb 550-szer','Kb 510-szer','Kb 10-szer'),
    new Question('Mennyi idő alatt fordul körbe saját tengelye körül a Neptunusz?','10 óra','18 óra','16 óra','20 óra'),
    new Question('Mit jelent a reguális szó?','rendezett','durva','edzett','mogorva'),
    new Question('Melyik a leghosszabb kígyó?','kockás piton','királysikló','tajpán','booslang'),
    new Question('Melyik a legnehezebb kígyó?','anakonda','boa','piton','kobra'),
    new Question('A "vízen kullogó" nevű süteménynek melyik nem összetevője?','őrőltbors','margarin','tojássárgája','tejföl'),
    new Question('Melyik nem a krumplis pogácsa összetevője?','cukor','sütőpor','zsír','tojás'),
    new Question('Hogy mondják másképpen a krumplis tésztát?','gránátos kocka','krumplis háromszög','bomba négyszög','krumplis kocka'),
    new Question('Mikor kezdődött az I. Világháború?','1914','1910','1814','1913'),
    new Question('Mikor kezdődött a Honfoglalás?','895','595','893','845'),
    new Question('Mikor fedezte fel Kolombusz Kristóf Amerikát?','1492','1392','1472','1590'),
    new Question('Az alábbiak közül melyik a tompaorr krokodil legkedveltebb élőhelye?','édes és sós vizek','édes vizek','sós vizek','szárazabb területek'),
    new Question('Melyik kígyó a világ legnagyobb méreg termelője?','gaboni vipera','kígyász sikló','boa','kockás piton'),
    new Question('Melyik elem vegyjele az "Yb"?','itterlisium','iridium','jód','kűrium'),
    new Question('Melyik elem vegyjele a "Cm"?','kűrium','kobalt','mendelévium','klór'),
    new Question('Mi vetett véget a Romanovok uralkodásának oroszországban?','az 1917-es forradalom','a II. világháború','kihalt a család','a Romanovok viszálya')    
];