import { IMessage } from "../../shared/imessage";

/**
 * Wildcard: <%wildcard_name%>
 * Usable wildcards:
 * answer_text      - the text of the selected answer
 * answer_letter    - the letter of the selected answer
 * correct_letter   - the letter of the correct answer
 * correct_text     - the text of the correct answer
 * earned_points    - the points earned(points given for the correctly answerd questions count)
 * 
 * For instance:
 * <%correct_letter%> will be replaced to the letter of the correct answer
 */

export let GameFailed: IMessage[][] = [
    [{text: 'Sajnálom, de a helyes válasz: <%correct_text%>', duration: 3000}],
    [{text: 'Legközelebb már tudni fogja!', duration: 3000}],
    [{text: 'Talán a másikat kellett volna megjelölnie!', duration: 3000}],
    [{text: 'Na ez most nem jött be!', duration: 3000}]
];

export let GameFailedWithFixed: IMessage[][] = [
    [
        {text: 'Sajnálom, de a helyes válasz: <%correct_text%>', duration: 2000},
        {text: 'De így is nyert <%earned_points%> pontot! Gratulálok hozzá!', duration: 3000}
    ],
    [
        {text: 'Sebaj! Legközelebb már tudni fogja!', duration: 2000},
        {text: 'Így most <%earned_points%> pontal távozhat! Gratulálok!', duration: 3000}        
    ],
    [
        {text: 'Talán a másikat kellett volna megjelölnie!', duration: 2000},
        {text: 'Így csak <%earned_points%> pontot vihet haza!', duration: 3000}
    ]
];

export let GameFailedStopped:  IMessage[][] = [
    [
        { text: 'Nem, nem <%answer_text%> a helyes válasz!', duration: 2500},
        { text: 'De mivel megállt így is nyert <%earned_points%> pontot! Gratulálok!', duration: 3000}
    ]
];

export let CorrectAnswerStopped:  IMessage[][] = [
    [
        { text: 'Milyen kár! Pedig jó lett volna!', duration: 2000},
        { text: 'De semmi vész! Így is nyert <%earned_points%> pontot!', duration: 3000}
    ]
];

export let StoppingConfirmed:  IMessage[][] = [
    [{ text: 'Rendben van. Akkor jelöljön meg egyet játékon kí­vül!', duration: 2500}]
];

export let CorrectAnswer:  IMessage[][] = [
    [{ text: 'Tökéletes választás!', duration: 2000}],
    [{ text: 'Hát persze hogy az!', duration: 2000}],
    [{ text: 'Már megyünk is tovább!', duration: 2000}],
    [{ text: 'Ez most bejött!', duration: 2000}]
];

export let GameStarting:  IMessage[][] = [
    [
        { text: 'Legyen Ön is milliomos!', duration: 2000 },
        { text: 'Lássuk az első kérdést!', duration: 2000 }
    ]
];

export let GrandPrizeWon:  IMessage[][] = [
    [
        { text: 'Ez hihetetlen!', duration: 1500},
        { text: 'Ön remekül játszott és megnyerte a főnyereményt! Őszintén gratulálok!', duration: 3000}
    ]
];