import { InjectionToken } from '@angular/core';
import { AppInfo } from '../shared/appinfo';


export const APP_INFO_TOKEN = new InjectionToken<AppInfo>("APP_INFO");

export const AppInfoConfig = {
    version: '2.0',
    author: 'Theobald Péter'
}