/**
 * Interface for defining a question
 */
export interface IQuestion {
    /**
     * The question to ask
     */
    Name: string;
    /**
     * The correct answert to the question
     */
    CorrectAnswer: string;
    /**
     * The first wrong answer
     */
    WrongAnswer1: string;
    /**
     * The second wrong answer
     */
    WrongAnswer2: string;
    /**
     * The third wrong answer
     */    
    WrongAnswer3: string;
}
