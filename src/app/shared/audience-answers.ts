
/**
 * Audience answers
 */
export class AudienceAnswers {
    AnswerA: number;
    AnswerB: number;
    AnswerC: number;
    AnswerD: number;
}
