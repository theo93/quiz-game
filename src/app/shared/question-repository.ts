import { IQuestion } from "../shared/iquestion";

export class QuestionRepository {
    QuestionPackLevel1: Array<IQuestion>;
    QuestionPackLevel2: Array<IQuestion>;
    QuestionPackLevel3: Array<IQuestion>;
    QuestionPackLevel4: Array<IQuestion>;
}