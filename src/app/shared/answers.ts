
/**
 * Mixed up answers
 */
export class Answers {
    AnswerA: string;
    AnswerB: string;
    AnswerC: string;
    AnswerD: string;
}
