
/**
 * Dynamic parameters for message evaluation.
 * Usage: <%property%> e.g: <%answer_test%>
 */
export interface IRandomMessageEvalParams {

    /**
     * The text of the last answer selected
     */
    answer_text: string;

    /**
     * The letter (e.g: A) of the last answer selected
     */
    answer_letter: string;

    /**
     * The letter (e.g: A) of the correct answer
     */
    correct_letter: string;

    /**
     * The text of the correct answer
     */
    correct_text: string;

    /**
     * Points actually earned by the user. If correct answer is selected the rewarded points are considered.
     */
    earned_points: number;
}