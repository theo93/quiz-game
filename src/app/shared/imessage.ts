import { IBoardAnimation } from "./board-animation";

export interface IMessage {
    // the message
    text: string;

    // the duration to show in miliseconds
    duration: number;

    // optional animations to run on the question board.
    anim?: IBoardAnimation
}