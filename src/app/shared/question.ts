import { IQuestion } from "./iquestion";

export class Question  implements IQuestion{
    Name: string;
    CorrectAnswer: string;
    WrongAnswer1: string;
    WrongAnswer2: string;
    WrongAnswer3: string;
    constructor(name: string, correctAnswer: string, wrongAnswer1: string, wrongAnswer2: string, wrongAnswer3: string) {
        this.Name = name;
        this.CorrectAnswer = correctAnswer;
        this.WrongAnswer1 = wrongAnswer1;
        this.WrongAnswer2 = wrongAnswer2;
        this.WrongAnswer3 = wrongAnswer3;
    }
}
