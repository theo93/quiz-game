export interface AppInfo {
    version: string;
    author: string;
}