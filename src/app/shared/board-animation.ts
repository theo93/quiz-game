export interface IBoardAnimation {
    
    /**
     * The answer letter to run the green blinking animation
     */
    BlinkGreen: string;

    /**
     * The answer letter to run the red blinking animation (optional)
     */   
    BlinkRed?: string;

}