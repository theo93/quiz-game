import { IMessage } from "./imessage";
import { IBoardAnimation } from "./board-animation";

export class MessagePack {

    private _messages: IMessage[] = [];

    public get Messages(): IMessage[] {
        return this._messages;
    }

    public static Create(messages: IMessage[]): MessagePack {
        let pack = new MessagePack();
        pack._messages = messages;
        return pack;
    }

    public Copy(): MessagePack {
        let pack = new MessagePack();
        pack._messages = this._messages.slice(0);
        return pack;
    }

    public WithDelayAndAnimation(delay: number, animation: IBoardAnimation): MessagePack {
        let pack = this.Copy();

        if (pack._messages[0]) {
            pack._messages[0].anim = animation;
        }
        pack._messages.unshift({text: null, duration: delay});

        return pack;
    }
}