import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'vertical-bar',
  templateUrl: './vertical-bar.component.html',
  styleUrls: ['./vertical-bar.component.scss']
})
export class VerticalBarComponent implements OnInit {

  public barHeight: number;
  public barText: string;
  
  constructor() { }

  /**
   * The value for the 100% in pixels
   */
  @Input() baseValue: number;

  /**
   * The current percent value to display
   */
  @Input() percent: number;

  ngOnInit() {
    this.barHeight = (this.baseValue / 100) * this.percent;
    this.barText = `${this.percent}%`;
  }

}
