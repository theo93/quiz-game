import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'graph-axis',
  templateUrl: './graph-axis.component.html',
  styleUrls: ['./graph-axis.component.scss']
})
export class GraphAxisComponent implements OnInit {

  /**
   * Valid values are 'x' and 'y'. Specifies the layout direction.
   */
  @Input() axisLayout: string;

  /**
   * The value for the very first position on the axis (usually 0)
   */
  @Input() startValue: number;

  /**
   * The value for the very last position on the axis
   */
  @Input() endValue: number;

  /**
   * Optional step value. The default step is 10% of the interval.
   */
  @Input() step: number;

  /**
   * The length of the axis. For the horizontal (axisKind=x) this is optional, and if missing it streches the whole viewport.
   */
  @Input() axisSize: string;

  /**
   * Custom values to lay out by the axsis.
   */
  @Input() stringValues: string[];

  @HostBinding('class.axis-x') get AxisX() {
    return this.axisLayout == 'x';
  }

  @HostBinding('class.axis-y') get AxisY() {
    return this.axisLayout == 'y';
  }

  @HostBinding('class.axis-numeric') get IsNumericAxis() {
    return typeof this.stringValues == 'undefined';
  }  

  public labels: string[] = []; 

  constructor() { }

  ngOnInit() {
    this.startValue = this.defaultIfUndefined('startValue', 0);
    this.endValue = this.defaultIfUndefined('endValue', 100);
    this.step = this.defaultIfUndefined('step', (this.endValue - this.startValue) / 10); 
    this.axisSize = this.defaultIfUndefined('axisSize', 'auto');
    
    if (typeof this.stringValues != 'undefined') {
      this.labels = this.stringValues.slice();
    } else {
      this.generateLabels();
    }
  }

  public getAxisSize() {
    if (this.axisSize == 'auto') {
      if (this.axisLayout == 'x') {
        return { 'width': '100%' };
      } else {
        return { 'height': '100%' };
      }
    } else {
      if (this.axisLayout == 'x') {
        return { 'width': this.axisSize };
      } else {
        return { 'height': this.axisSize };
      }     
    }
  }

  public getLabelPosition(index) {
    if (index == 0 || index == this.labels.length - 1) {
      return {};
    }


    return this.axisLayout == 'x' ? { 'left.%': index / (this.labels.length - 1) * 100 } :{ 'top.%': index / (this.labels.length -1) * 100 };
  }

  private generateLabels() {
    this.labels = [];
    if (typeof this.startValue != 'number' || typeof this.endValue != 'number') {
      return;
    }
    for(let i = this.startValue; i <= this.endValue; i+=this.step) {
      this.labels.push(i.toString());
    }

    if (this.axisLayout == 'y') {
      this.labels.reverse();
    }
  }

  private defaultIfUndefined(propertyName: string, defaultValue: any): any {
    return typeof this[propertyName] == 'undefined' ? defaultValue : this[propertyName];
  }  

}
