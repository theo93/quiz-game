import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphAxisComponent } from './graph-axis.component';

describe('GraphAxisComponent', () => {
  let component: GraphAxisComponent;
  let fixture: ComponentFixture<GraphAxisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphAxisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphAxisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
