import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudienceModalComponent } from './audience-modal.component';

describe('ModalComponent', () => {
  let component: AudienceModalComponent;
  let fixture: ComponentFixture<AudienceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudienceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudienceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
