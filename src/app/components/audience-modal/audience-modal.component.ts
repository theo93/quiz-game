import { Component, OnInit, Inject } from '@angular/core';
import { AudienceAnswers } from '../../shared/audience-answers';

import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'audience-modal',
  templateUrl: './audience-modal.component.html',
  styleUrls: ['./audience-modal.component.scss']
})
export class AudienceModalComponent implements OnInit {


  public audienceAnswers: number[];

  constructor(@Inject(MAT_DIALOG_DATA) answers: AudienceAnswers) { 
    this.PrepareAudienceAnswers(answers);
  }

  ngOnInit() {
  }

  private PrepareAudienceAnswers(answers: AudienceAnswers) {
    this.audienceAnswers =[];
    for(let i in answers) {
      this.audienceAnswers.push(answers[i]);
    }
  }



}
