import { Component, OnInit, Input } from '@angular/core';
import { IScoreItem } from '../../shared/iscore-item';
import { ScoreService } from '../../services/score-service/score-service.service';

@Component({
  selector: 'score-board',
  templateUrl: './score-board.component.html',
  styleUrls: ['./score-board.component.scss']
})
export class ScoreBoardComponent implements OnInit {

  @Input() earnedScore: number;

  constructor(private scoreService: ScoreService) { }

  scoreList: Array<IScoreItem>;

  ngOnInit() {
    this.scoreList = this.scoreService.GetScoreListForRendering();

  }

  getClass(index): string {

    let classes: string = '';
    let current = 14 - index;
    if (current > this.earnedScore) {
      classes += 'glyphicon-remove text-muted';
    } else if(current < this.earnedScore) {
      classes += 'glyphicon-ok text-success';
    } else if(current == this.earnedScore) {
      classes += 'glyphicon-arrow-right text-warning';
    }

      return classes;
  }

}
