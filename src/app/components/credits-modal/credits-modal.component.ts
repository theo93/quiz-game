import { Component, OnInit, Inject } from '@angular/core';
import { AppInfo } from '../../shared/appinfo';
import { APP_INFO_TOKEN } from '../../config/app-info';

@Component({
  selector: 'credits-modal',
  templateUrl: './credits-modal.component.html',
  styleUrls: ['./credits-modal.component.scss']
})
export class CreditsModalComponent implements OnInit {

  constructor(@Inject(APP_INFO_TOKEN) public  appInfo: AppInfo) { }

  ngOnInit() {
  }

}
