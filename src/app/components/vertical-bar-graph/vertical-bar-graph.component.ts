import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'vertical-bar-graph',
  templateUrl: './vertical-bar-graph.component.html',
  styleUrls: ['./vertical-bar-graph.component.scss']
})
export class VerticalBarGraphComponent implements OnInit {

@Input() values: number[];
@Input() baseValue: number; // LIMIT its a px valut! :/

@Input() step: number;

public yAxisSize: number;
public yAxisEndValue: number;
  constructor() { }



  ngOnInit() {
    this.baseValue = this.defaultIfUndefined('baseValue', 200);
    this.step = this.defaultIfUndefined('step', 15);
    this.values = this.defaultIfUndefined('values', [50, 25, 10, 15]); // DUMMY values if not provided
    this.calculateYAxis();
  }

  private calculateYAxis() {
    let max = Math.max(...this.values);
    this.yAxisEndValue = (Math.floor(max / this.step) + 2) * this.step;
    this.yAxisSize = (this.baseValue / 100) * this.yAxisEndValue;
  }

  private defaultIfUndefined(propertyName: string, defaultValue: any): any {
    return typeof this[propertyName] == 'undefined' ? defaultValue : this[propertyName];
  }  
}
