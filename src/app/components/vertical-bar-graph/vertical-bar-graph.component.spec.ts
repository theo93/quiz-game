import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerticalBarGraphComponent } from './vertical-bar-graph.component';

describe('VerticalBarGraphComponent', () => {
  let component: VerticalBarGraphComponent;
  let fixture: ComponentFixture<VerticalBarGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerticalBarGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalBarGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
