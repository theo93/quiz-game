import { trigger, state, style, animate, animation, transition, keyframes } from '@angular/animations';

/**
 * Taken from the apropriate Material Design palettes
 */
const Colors = {
    Green: '#4caf50', // $mat-green, 500
    Red: '#e53935' // $mat-green, 600
}

export let blinkAnimation = trigger('blinkAnimation',[
    state('green', style({
        backgroundColor: Colors.Green,
    })),
    state('slowGreen', style({
        backgroundColor: Colors.Green,
    })),    
    state('red', style({
        backgroundColor: Colors.Red,
    })),    
    transition("* => green", [
        animate('.9s linear', keyframes([
            style({
                backgroundColor: Colors.Green,
                offset: 0.1
            }),
            style({
                backgroundColor: '*',
                offset: 0.4
            }),
            style({
                backgroundColor: Colors.Green,
                offset: 0.6
            }),
            style({
                backgroundColor: '*',
                offset: 0.9
            })
        ]))
    ]),
    transition("* => slowGreen", [
        animate('1.6s linear', keyframes([
            style({
                backgroundColor: Colors.Green,
                offset: 0.1
            }),
            style({
                backgroundColor: '*',
                offset: 0.4
            }),
            style({
                backgroundColor: Colors.Green,
                offset: 0.6
            }),
            style({
                backgroundColor: '*',
                offset: 0.9
            })
        ]))
    ]),    
    transition("* => red", [
        animate('1.6s linear', keyframes([
            style({
            backgroundColor: Colors.Red,
            offset: 0.1
            }),
            style({
            backgroundColor: '*',
            offset: 0.4
            }),
            style({
            backgroundColor: Colors.Red,
            offset: 0.6
            }),
            style({
            backgroundColor: '*',
            offset: 0.9
            })
        ]))
    ])
])
