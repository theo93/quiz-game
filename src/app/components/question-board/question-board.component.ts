import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { IQuestion } from '../../shared/iquestion';
import { Answers } from '../../shared/answers';
import { MessageService } from '../../services/message-service/message-service.service';
import { Observable } from 'rxjs/Observable';
import { IMessage } from '../../shared/imessage';
import { blinkAnimation } from './question-board.animation';
import { QuestionService } from '../../services/question-service/question.service';

@Component({
  selector: 'question-board',
  templateUrl: './question-board.component.html',
  styleUrls: ['./question-board.component.scss'],
  animations:[ blinkAnimation ]
})
export class QuestionBoardComponent implements OnInit, OnChanges {

  @Input() question: string;
  @Input() answers: Answers;

  @Output() answerSelected: EventEmitter<string> = new EventEmitter<string>();

  questionText: string = null;

  // for animations
  blinkAnimation: Answers = new Answers();
  selectedLetter: string;

  constructor(private messageService: MessageService, private questionService: QuestionService) { }

  ngOnInit() {
    if (this.question && this.answers) {
      this.writeQuestion();
    }

    this.messageService.OnMessagePack.subscribe((messagePack: Observable<IMessage>) => {
      let packObserver = messagePack.subscribe(message => {
        if (message.text == null) {
          this.writeQuestion();
        } else {
          this.questionText = message.text;
        }
        if (typeof message.anim !== 'undefined') {

          let isRed = typeof message.anim.BlinkRed !== 'undefined';

          this.blinkAnswer(message.anim.BlinkGreen, isRed ? 'slowGreen' : 'green');
          if (isRed) {
            this.blinkAnswer(message.anim.BlinkRed, 'red');
          }
        }
      },
      null,
       () => {
         if (!this.isAnimationsActive()) {
           this.writeQuestion();
         }
        packObserver.unsubscribe();
      });
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.clearAnimations();
    if (this.question && this.answers) {
      this.writeQuestion();
    } else {
      this.clearBoard();
    }
  }  

  public onAnswerClick(answerLetter: string) {
      this.selectedLetter = answerLetter;
      this.answerSelected.emit(this.selectedLetter);
  }
  
  private writeQuestion() {
    this.questionText = this.question;
  }
  
  private blinkAnswer(answerLetter: string, animationName: string) {
     this.blinkAnimation['Answer' + answerLetter] = animationName;
  }

  private clearBoard() {
    this.questionText = null;
    this.answers = new Answers();
  }

  private clearAnimations() {
    this.blinkAnimation = new Answers();
    this.selectedLetter = null;
  }

  private isAnimationsActive(): boolean {
    
    let keys = Object.keys(this.blinkAnimation);
    return keys.map(property => typeof this.blinkAnimation[property] != 'undefined')
    .reduce((prev, curr) => prev || curr, false);
    
  }  

}
