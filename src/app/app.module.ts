import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';

import { AppComponent } from './app.component';
import { ScoreBoardComponent } from './components/score-board/score-board.component';
import { QuestionBoardComponent } from './components/question-board/question-board.component';
import { QuestionService } from './services/question-service/question.service';
import { GameManager } from './services/game-manager/game-manager';
import { MessageService } from './services/message-service/message-service.service';
import { ScoreService } from './services/score-service/score-service.service';
import { RandomMessageService } from './services/random-message/random-message.service';
import { GameAidService } from './services/game-aid/game-aid.service';
import { AudienceModalComponent } from './components/audience-modal/audience-modal.component';
import { VerticalBarComponent } from './components/vertical-bar/vertical-bar.component';
import { GraphAxisComponent } from './components/graph-axis/graph-axis.component';
import { VerticalBarGraphComponent } from './components/vertical-bar-graph/vertical-bar-graph.component';
import { AppInfoConfig, APP_INFO_TOKEN } from './config/app-info';
import { CreditsModalComponent } from './components/credits-modal/credits-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    ScoreBoardComponent,
    QuestionBoardComponent,
    AudienceModalComponent,
    VerticalBarComponent,
    GraphAxisComponent,
    VerticalBarGraphComponent,
    CreditsModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatDividerModule
  ],
  providers: [
    QuestionService,
    GameManager,
    MessageService,
    ScoreService,
    RandomMessageService,
    GameAidService,
    {provide: APP_INFO_TOKEN, useValue: AppInfoConfig}
  ],
  entryComponents: [AudienceModalComponent, CreditsModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
