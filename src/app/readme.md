# Readme

This repository contains the sourcecode of  an Angular 5 quiz game implementation.

## Planned features

* In case of a winned game there should be a reward animation (e.g: that runs on the question board)
* There should be 'AI' messages specifically when you pass a fixed prize question.
* The audience modal can be shown a little bit later with extra 'AI' messages before it.(The messages can be adressed to the finctional audience)
* There can be an alternate design the user can switch to
* Further responsive optimisations to support mobile gameing.