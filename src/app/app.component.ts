import { Component, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { GameManager } from './services/game-manager/game-manager';
import { QuestionService } from './services/question-service/question.service';
import { AppInfo } from './shared/appinfo';
import { AudienceModalComponent } from './components/audience-modal/audience-modal.component';
import { CreditsModalComponent } from './components/credits-modal/credits-modal.component';
import { APP_INFO_TOKEN } from './config/app-info';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public gameManager: GameManager,
    public questionService: QuestionService,
    private dialogService: MatDialog,
    @Inject(APP_INFO_TOKEN) public  appInfo: AppInfo) {
    gameManager.IsDebug = false;
  }

  OpenAudienceModal() {
    let answers = this.gameManager.ActivateAudience();
    this.dialogService.open(AudienceModalComponent, { minWidth: '60%', panelClass: 'no-padding-dialog', data: answers});
  }

  OpenCreditsModal() {
    this.dialogService.open(CreditsModalComponent,{ minWidth: '60%', panelClass: 'primary-background-dialog'});
  }

}
