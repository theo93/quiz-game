
import { Injectable } from '@angular/core';
import { ScoreList } from '../../config/score-list';
import { IScoreItem } from '../../shared/iscore-item';

@Injectable()
export class ScoreService {
  private scoreList: IScoreItem[] = ScoreList;
  constructor() { }

  GetScoreListForRendering(): IScoreItem[] {
    return this.scoreList.slice().reverse();
  }

  GetScoreForCorrectAnswerCount(correctAnswers: number): number {
    if (correctAnswers < 1) {
      return 0;
    }
    return this.scoreList[correctAnswers - 1].value;
  }
}
