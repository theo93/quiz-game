import { TestBed, inject } from '@angular/core/testing';

import { ScoreService } from './score-service.service';

describe('ScoreServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScoreService]
    });
  });

  it('should be created', inject([ScoreService], (service: ScoreService) => {
    expect(service).toBeTruthy();
  }));
});
