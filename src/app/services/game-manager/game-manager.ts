import { IQuestion } from "../../shared/iquestion";
import { QuestionService } from "../question-service/question.service";
import { Injectable } from "@angular/core";
import { MessageService } from "../message-service/message-service.service";
import { ScoreService } from "../score-service/score-service.service";
import { Answers } from "../../shared/answers";
import { RandomMessageService } from "../random-message/random-message.service";
import { MessagePack } from "../../shared/message-pack";
import { IRandomMessageEvalParams } from "../../shared/irandom-message-eval-params";
import { GameAidService } from "../game-aid/game-aid.service";
import { AudienceAnswers } from "../../shared/audience-answers";

@Injectable()
export class GameManager {

    private _answerdQuestionCount: number;
    private _isGameStarted: boolean;
    private _isRiskingEnabled: boolean;
    private _question: IQuestion;
    private _level: number;

    private _mixedAnswers: Answers;
    private _correctLetter: string;

    public IsDebug: boolean = false;

    public constructor(private questionService: QuestionService,
        private messageService: MessageService,
        private scoreService: ScoreService,
        private randomMessageService: RandomMessageService,
        private gameAidService: GameAidService
    ) {
        this.ResetGame();
    }

    /** Getters for outside use */
    public get AnsweredQuestionCount() {
        return this._answerdQuestionCount;
    }

    public get CurrentQuestion() {
        return this._question ? this._question.Name : '';
    }
    
    public get CurrentAnswers() {
        return this._mixedAnswers;
    }    

    public get IsGameStarted() {
        return this._isGameStarted;
    }

    public get IsRiskingEnabled() {
        return this._isRiskingEnabled;
    }    

    public getDebugClass(): string {
        if (!this._question || !this.IsDebug) {
            return null;
        }

        return 'highlight-' + this._correctLetter.toLocaleLowerCase();         
    }

    public get IsHalflingsAvailable() {
        return this.gameAidService.HalflingAvailable;
    }

    public get IsAudienceAvailable() {
        return this.gameAidService.AudienceAvailable;
    }

    public ActivateHalflings() {
        this._mixedAnswers = this.gameAidService.UseHalfling(this._mixedAnswers, this._correctLetter);
    }

    public ActivateAudience(): AudienceAnswers {
        return this.gameAidService.UseAudience(this._mixedAnswers, this._correctLetter, this._level);
    }

    public StartGame() {

        if (this.IsGameStarted) {
            return;
        }
        this.ResetGame();

        this._answerdQuestionCount++;
        this._isGameStarted = true;
        this.messageService.SendMessage(this.randomMessageService.GetMessageTemplate('GameStarting', this.createEvalParameters('')))
        .subscribe(null, null,
            () => {
                this.questionService.SetQuestionPack(this._level);
                this._question = this.questionService.GetRandomQuestion();
                this._mixedAnswers = this.questionService.GetMixedAnswers(this._question);
                this.CalculateCorrectLetter();
            }
        );
    }

    public StopGame() {
        let confirmed = confirm('Biztosan meg akar állni?'); // TODO better (e.g. show in questionboard)
        if (confirmed) {
            this._isRiskingEnabled = false;
            this.messageService.SendMessage(this.randomMessageService.GetMessageTemplate('StoppingConfirmed', this.createEvalParameters('')));
        }
    }

    public OnAnswerSelection(answerLetter: string) {

        if (this._isRiskingEnabled) {
            if (answerLetter == this._correctLetter) {
                this.StepGame(answerLetter);
            } else {
                this.FailGame(answerLetter);
            }
        } else {
            if (answerLetter == this._correctLetter) {

                this.messageService.SendMessage(this.randomMessageService.GetMessageTemplate('CorrectAnswerStopped', this.createEvalParameters(answerLetter)).WithDelayAndAnimation(2000, { BlinkGreen: answerLetter }))
                .subscribe(null, null,
                    () => {
                        this.ResetGame();
                    }
                );
            } else {
                this.messageService.SendMessage(this.randomMessageService.GetMessageTemplate('GameFailedStopped', this.createEvalParameters(answerLetter)).WithDelayAndAnimation(2000, { BlinkGreen: answerLetter }))
                .subscribe(null, null,
                    () => {
                        this.ResetGame();
                    }
                );                
            }            
        }
    }

    private GetFixPoints(): number {
        let points = 0;
        if(this._answerdQuestionCount >= 5) {
            let rem = this._answerdQuestionCount % 5;
            points = this.scoreService.GetScoreForCorrectAnswerCount(this._answerdQuestionCount - rem);
        }

        return points;
    }

    private CalculateCorrectLetter() {
        switch (this._question.CorrectAnswer) {
            case this._mixedAnswers.AnswerA : 
                this._correctLetter = 'A';
                break;
            case this._mixedAnswers.AnswerB : 
                this._correctLetter = 'B';
                break;
            case this._mixedAnswers.AnswerC : 
                this._correctLetter = 'C';
                break;
            case this._mixedAnswers.AnswerD : 
                this._correctLetter = 'D';
                break;
        }
    }

    private FailGame(answerLetter: string) {

        let fixAward = this.GetFixPoints();

        let templateName = fixAward > 0 ? 'GameFailedWithFixed' : 'GameFailed';

        this.messageService.SendMessage(this.randomMessageService.GetMessageTemplate(templateName, this.createEvalParameters(answerLetter, fixAward)).WithDelayAndAnimation(2000, {BlinkGreen: this._correctLetter, BlinkRed: answerLetter }))
        .subscribe(null, null,
            () => {
                this.ResetGame();
            }
        );
    }

    private StepGame(answerLetter: string) {

        if (this._answerdQuestionCount == 14) {

            this.messageService.SendMessage(this.randomMessageService.GetMessageTemplate('GrandPrizeWon', this.createEvalParameters(answerLetter)).WithDelayAndAnimation(2000, { BlinkGreen: answerLetter }))
            .subscribe(null, null, () => {
                this.ResetGame();
            });

        } else {

            this.messageService.SendMessage(this.randomMessageService.GetMessageTemplate('CorrectAnswer', this.createEvalParameters(answerLetter)).WithDelayAndAnimation(2000, { BlinkGreen: answerLetter }))
            .subscribe(null, null, () => {
                this._answerdQuestionCount++;
                if ((this._answerdQuestionCount + 1) % 5 == 0) {
                    this._level++;
                    this.questionService.SetQuestionPack(this._level);
                }
                this._question = this.questionService.GetRandomQuestion();
                this._mixedAnswers = this.questionService.GetMixedAnswers(this._question);
                this.CalculateCorrectLetter();
            });

        }
    }

    private createEvalParameters(answer_letter: string, earned_points?: number): IRandomMessageEvalParams {
        
        let earned = earned_points || this.scoreService.GetScoreForCorrectAnswerCount(this._answerdQuestionCount);
        return {
                answer_text: this._mixedAnswers ? this._mixedAnswers['Answer' + answer_letter] : undefined,
                answer_letter: answer_letter,
                correct_letter: this._correctLetter,
                correct_text: this._question ? this._question.CorrectAnswer : undefined,
                earned_points: earned
            }
    }

    private ResetGame() {
        this._answerdQuestionCount = -1;
        this._isGameStarted = false;
        this._isRiskingEnabled = true;
        this._question = null;
        this._level = 1;
        this.gameAidService.Reset();
    }
}