import { Injectable } from '@angular/core';
import { IQuestion } from '../../shared/iquestion';
import { QuestionRepository } from '../../shared/question-repository';
import { Questions as QLevel1 } from '../../config/questions/questions-level1';
import { Questions as QLevel2 } from '../../config/questions/questions-level2';
import { Questions as QLevel3 } from '../../config/questions/questions-level3';
import { Questions as QLevel4 } from '../../config/questions/questions-level4';
import { Answers } from '../../shared/answers';

@Injectable()
export class QuestionService {
  
  private questionRepository: QuestionRepository;
  private _questionPack: IQuestion[];
  private _answerdQuestionInPack: number[];

  constructor() { 
    this.LoadRepository();
  }
  
  public SetQuestionPack(level: number): void {
    this._questionPack = <Array<IQuestion>>this.questionRepository['QuestionPackLevel'+ level];
    this._answerdQuestionInPack = [];
  }

  public get TotalQuestionCount() {
    let count = 0;
    for (let i in this.questionRepository) {
      count += this.questionRepository[i].length;
    }
    return count;
  }

  public GetMixedAnswers(question: IQuestion): Answers {


    let lettersUsed = [];
    let answers = new Answers();

    // place the correct answer to the right place
    let correctAnswerLetter = this.getRandomLetter();
    lettersUsed.push(correctAnswerLetter);
    answers['Answer'+ correctAnswerLetter] = question.CorrectAnswer;

    let currentWrongLetter;
    let currentWrongAnswer = 1;

    // mix up the wrong answers
    while(currentWrongAnswer<4) {
      currentWrongLetter = this.getRandomLetter();
      while (lettersUsed.includes(currentWrongLetter)) {
        currentWrongLetter = this.getRandomLetter();
      }
      lettersUsed.push(currentWrongLetter);
      answers['Answer'+ currentWrongLetter] = question['WrongAnswer' + currentWrongAnswer];
      currentWrongAnswer++;
    }

    return answers;
  }

  private getRandomLetter(): string {
    let letters = ['A', 'B', 'C', 'D'];
    let i = Math.floor(Math.random() * 4);
    return letters[i];
  }  

  public GetRandomQuestion(): IQuestion {
    let question: IQuestion;
    let rndIndex;

    // in this case there is no way we can find a random question
    if (this._questionPack.length <= this._answerdQuestionInPack.length)
    {
     return null;
    }
    do {
        rndIndex = Math.floor(Math.random() * this._questionPack.length);
    } while(this._answerdQuestionInPack.includes(rndIndex));

    question = this._questionPack[rndIndex];
    this._answerdQuestionInPack.push(rndIndex);
    return question;
}    

  private LoadRepository() {
    this.questionRepository = new QuestionRepository();
    this.questionRepository.QuestionPackLevel1 = QLevel1;
    this.questionRepository.QuestionPackLevel2 = QLevel2;
    this.questionRepository.QuestionPackLevel3 = QLevel3;
    this.questionRepository.QuestionPackLevel4 = QLevel4;
  }
}
