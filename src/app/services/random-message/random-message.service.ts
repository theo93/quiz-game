import { Injectable } from '@angular/core';
import { IRandomMessageEvalParams } from '../../shared/irandom-message-eval-params';
import { IMessage } from '../../shared/imessage';
import * as MessageTemplates from '../../config/message-templates';
import { MessagePack } from '../../shared/message-pack';

@Injectable()
export class RandomMessageService {

  constructor() { }

  // TODO enum
  GetMessageTemplate(type: string, evalParams: IRandomMessageEvalParams): MessagePack {
    
    let pack: MessagePack = null;
    
    if (typeof MessageTemplates[type] != 'undefined') {
      let templates = <IMessage[][]>MessageTemplates[type];
      let len = templates.length;

      let rndInd = Math.floor(Math.random() * len);

      let evaledMessages = this.evalMessages(templates[rndInd], evalParams);
      pack = MessagePack.Create(evaledMessages);
    }

    return pack;
  }

  private evalMessages(messages: IMessage[], evalParams: IRandomMessageEvalParams): IMessage[] {
    let retVal: IMessage[] = messages.map((msg: IMessage)=>{
      let cpyMessage: IMessage = Object.assign({}, msg);
      cpyMessage.text = this.evalMessage(cpyMessage.text, evalParams);
      return cpyMessage;
    });
    return retVal;
  }

  private evalMessage(text: string, evalParams: IRandomMessageEvalParams): string {
    return text.replace(/<%([a-zA-Z_]+)%>/g, (match: string, g1: string) => {
      if (typeof evalParams[g1] !='undefined') {
        return evalParams[g1];
      } else {
        return match;
      }
    });
  }
}
