import { TestBed, inject } from '@angular/core/testing';

import { RandomMessageService } from './random-message.service';

describe('RandomMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RandomMessageService]
    });
  });

  it('should be created', inject([RandomMessageService], (service: RandomMessageService) => {
    expect(service).toBeTruthy();
  }));
});
