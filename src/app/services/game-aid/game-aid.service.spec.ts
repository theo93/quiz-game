import { TestBed, inject } from '@angular/core/testing';

import { GameAidService } from './game-aid.service';

describe('AidActionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameAidService]
    });
  });

  it('should be created', inject([GameAidService], (service: GameAidService) => {
    expect(service).toBeTruthy();
  }));
});
