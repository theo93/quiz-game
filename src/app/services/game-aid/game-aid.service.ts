import { Injectable } from '@angular/core';
import { Answers } from '../../shared/answers';
import { AudienceAnswers } from '../../shared/audience-answers';

@Injectable()
export class GameAidService {

  constructor() { }

  private _halflingAvailable: boolean;
  private _audienceAvailable: boolean;

  public get HalflingAvailable() {
    return this._halflingAvailable;
  }

  public get AudienceAvailable() {
    return this._audienceAvailable;
  }  

  public Reset() {
    this._halflingAvailable = true;
    this._audienceAvailable = true;
  }

  public UseHalfling(answers: Answers, correctLetter: string): Answers {
    let retVal: Answers = new Answers();
    Object.assign(retVal, answers);

    if (!this._halflingAvailable) {
      return retVal;
    }
    
    let letters = ['A','B', 'C', 'D'];    
    let count = 0;
    do {
      let rndIndex = Math.floor(Math.random() * letters.length);
      let answerLetter = letters[rndIndex];
      if (answerLetter != correctLetter && retVal['Answer' + answerLetter] != null) {
        count++;
        retVal['Answer' + answerLetter] = null;
      }
    } while(count<2);

    this._halflingAvailable = false;
    return retVal
  }

  public UseAudience(answers: Answers, correctLetter: string, level: number): AudienceAnswers {
    
    let retVal: AudienceAnswers = new AudienceAnswers();
    
    if (!this._audienceAvailable) {
      return retVal;
    }

    
    let voteCountA = Math.floor(999 * Math.random()) +1;
    let voteCountB = Math.floor(999 * Math.random()) +1;
    let voteCountC = Math.floor(999 * Math.random()) +1;
    let voteCountD = Math.floor(999 * Math.random()) +1;
    
    let kknowV=Math.floor(99 * Math.random()) +1;
    let correctSelected=false;
    
    if (level==1 && kknowV<=58) {        
      correctSelected=true;        
    } else if (level==2 && kknowV<=25) {        
      correctSelected=true;        
    } else if(level==3 && kknowV<=17) {        
      correctSelected=true;      
    }
  
  
    if (correctLetter=='A' && correctSelected) {
      while (voteCountA != Math.max(voteCountA, voteCountB, voteCountC, voteCountD)) {        
        voteCountA = Math.floor(999 * Math.random()) +1;  
      }      
    } else if (correctLetter=='B' && correctSelected) {    
      while (voteCountB != Math.max(voteCountA, voteCountB, voteCountC, voteCountD)) {        
        voteCountB = Math.floor(999 * Math.random()) +1;  
      }      
    } else if (correctLetter=='C' && correctSelected) {    
      while (voteCountC != Math.max(voteCountA, voteCountB, voteCountC, voteCountD)) {        
        voteCountC = Math.floor(999 * Math.random()) +1;  
      }    
    } else if (correctLetter=='D' && correctSelected) {      
      while (voteCountD != Math.max(voteCountA, voteCountB, voteCountC, voteCountD)) {        
        voteCountD = Math.floor(999 * Math.random()) +1;  
      }      
    }    
  
    let hundredpercent = voteCountA + voteCountB + voteCountC + voteCountD;

    retVal.AnswerA = Math.round(voteCountA / (hundredpercent / 100));
    retVal.AnswerB = Math.round(voteCountB / (hundredpercent / 100));
    retVal.AnswerC = Math.round(voteCountC / (hundredpercent / 100));
    retVal.AnswerD = Math.round(voteCountD / (hundredpercent / 100));      
    
    this._audienceAvailable=false;
    
    return retVal;
    
  }

}
