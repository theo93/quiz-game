import { Injectable } from '@angular/core';
import { IMessage } from '../../shared/imessage';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';
import { MessagePack } from '../../shared/message-pack';

@Injectable()
export class MessageService {

  public OnMessagePack: Observable<Observable<IMessage>>;
  private _subject: Subject<Observable<IMessage>>;

  constructor() { 
    this._subject = new Subject();
    this.OnMessagePack = this._subject.asObservable();
  }


  SendMessage(messages: MessagePack): Observable<IMessage> {
    let observable = this.createObservable(messages);
    this._subject.next(observable);
    return observable;
  }


  private createObservable(messages: MessagePack): Observable<IMessage> {
    let observable = Observable.create((observer: Observer<IMessage>) => {

      if (messages.Messages.length == 0) {
        return observer.complete();
      }

      observer.next(messages.Messages[0]);
      emitAfterTimeout(messages.Messages, 1);

      function emitAfterTimeout(array: IMessage[], index: number) {
        setTimeout(() => {
          if (index < array.length) { // in case of there is only one element in the array
            observer.next(array[index]);
          } else {
            return observer.complete();
          }
          
          emitAfterTimeout(array, index + 1);          

        }, array[index - 1].duration);

      }

    });
    return observable;
  }

}
